import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutterapp/routes.dart';
import 'modifier_utilisateurs.dart';

class AffichageUtilisateurs extends StatefulWidget {
  const AffichageUtilisateurs({Key? key}) : super(key: key);

  @override
  State<AffichageUtilisateurs> createState() => _AffichageUtilisateursState();
}

class _AffichageUtilisateursState extends State<AffichageUtilisateurs> {
  late Future<List> _bookList;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _bookList = Utilisateurs.getAllUtilisateurs();
  }

  @override
  Widget build(BuildContext context) {
    if (ModalRoute.of(context)!.settings.arguments != null) {
      Object? arg = ModalRoute.of(context)!.settings.arguments;
      var newUtilisateurs = jsonDecode(arg.toString());
      setState(() {
        _bookList = _bookList.then<List>((value) {
          return [newUtilisateurs, ...value];
        });
      });
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text("Liste des Utilisateurs"),
      ),
      body: Container(
        child: FutureBuilder<List>(
          future: _bookList,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: snapshot.data?.length,
                  itemBuilder: (context, i) {
                    return Card(
                      child: ListTile(
                        title: Text(
                          snapshot.data![i]['nomUtilisateur'],
                          style: const TextStyle(fontSize: 30),
                        ),
                        subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Text(snapshot.data![i]['prenomUtilisateur'],
                                  style: const TextStyle(fontSize: 20)),
                              Text(snapshot.data![i]['mail'],
                                  style: const TextStyle(fontSize: 20)),
                              Text(snapshot.data![i]['role'],
                                  style: const TextStyle(fontSize: 20)),
                            ]),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            IconButton(
                              icon: Icon(Icons.edit),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ModifierUtilisateur(
                                          ref: int.parse(snapshot.data![i]["id"]
                                              .toString()))),
                                );
                              },
                            ),
                            IconButton(
                              icon: Icon(Icons.delete),
                              onPressed: () async {
                                int ref = snapshot.data![i]['id'];
                                print(ref);
                                await Utilisateurs.deleteUtilisateur(
                                    context, ref);
                                setState(() {
                                  _bookList = _bookList.then<List>((value) {
                                    return value
                                        .where(
                                            (element) => element['id'] != ref)
                                        .toList();
                                  });
                                });
                              },
                            ),
                          ],
                        ),
                      ),
                    );
                  });
            } else {
              return const Center(
                child: Text("Pas de données"),
              );
            }
          },
        ),
      ),
    );
  }
}
