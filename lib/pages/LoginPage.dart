import 'package:flutter/material.dart';
import 'package:flutterapp/routes.dart';
import 'package:snippet_coder_utils/FormHelper.dart';
import 'package:snippet_coder_utils/ProgressHUD.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isAPIcallProcess = false;
  bool hidePassword = true;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();
  String? email;
  String? password;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.yellow,
        body: ProgressHUD(
          child: Form(
            key: globalFormKey,
            child: _loginUI(context),
          ),
          inAsyncCall: isAPIcallProcess,
          opacity: 0.3,
          key: UniqueKey(),
        ),
      ),
    );
  }

  Widget _loginUI(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.only(left: 20, top: 90, bottom: 30),
              child: Text(
                "Connexion :",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Colors.black),
              ),
            ),
            FormHelper.inputFieldWidget(context, "Email", "Email",
                (onValidateVal) {
              if (onValidateVal.isEmpty) {
                return "L'email ne peut être vide";
              }
              return null;
            }, (onSaved) {
              email = onSaved;
            },
                borderFocusColor: Colors.black,
                borderColor: Colors.black,
                textColor: Colors.black,
                hintColor: Colors.black.withOpacity(0.8),
                borderRadius: 10),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: FormHelper.inputFieldWidget(
                context,
                "Mot_de_passe",
                "mot de passe",
                (onValidateVal) {
                  if (onValidateVal.isEmpty) {
                    return "Le mot de passe ne peut être vide";
                  }
                  return null;
                },
                (onSaved) {
                  password = onSaved;
                },
                borderFocusColor: Colors.black,
                borderColor: Colors.black,
                textColor: Colors.black,
                hintColor: Colors.black.withOpacity(0.8),
                borderRadius: 10,
                obscureText:
                    hidePassword, // Utilise la valeur de hidePassword pour cacher ou montrer le mot de passe
                suffixIcon: IconButton(
                  icon: Icon(
                      hidePassword ? Icons.visibility : Icons.visibility_off),
                  onPressed: () {
                    setState(() {
                      hidePassword =
                          !hidePassword; // Inverse la valeur de hidePassword
                    });
                  },
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Center(
              child: FormHelper.submitButton("login", () {
                dynamic validate = globalFormKey.currentState?.validate();
                if (validate != null && validate) {
                  globalFormKey.currentState?.save();
                  Utilisateurs.Login(context, email, password);
                }
              },
                  btnColor: Colors.yellow,
                  borderColor: Colors.black,
                  txtColor: Colors.black,
                  borderRadius: 10),
            )
          ]),
    );
  }
}
