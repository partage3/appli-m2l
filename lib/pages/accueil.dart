import 'package:flutter/material.dart';
import 'package:flutterapp/routes.dart';
import 'package:snippet_coder_utils/FormHelper.dart';

class Acceuil extends StatefulWidget {
  const Acceuil({Key? key}) : super(key: key);

  @override
  State<Acceuil> createState() => _AcceuilState();
}

class _AcceuilState extends State<Acceuil> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Accueil"),
      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(left: 125, top: 70),
            child: FormHelper.submitButton("Produits", () {
              Navigator.pushNamed(context, '/produits');
            },
                btnColor: Colors.yellow,
                borderColor: Colors.black,
                txtColor: Colors.black,
                borderRadius: 10),
          ),
          Padding(
            padding: EdgeInsets.only(left: 125, top: 70),
            child: FormHelper.submitButton("Utilisateurs", () {
              Navigator.pushNamed(context, '/utilisateurs');
            },
                btnColor: Colors.yellow,
                borderColor: Colors.black,
                txtColor: Colors.black,
                borderRadius: 10),
          )
        ],
      ),
    );
  }
}
