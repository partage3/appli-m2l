import 'package:flutter/material.dart';
import 'package:flutterapp/routes.dart';
import 'package:snippet_coder_utils/FormHelper.dart';
import 'package:snippet_coder_utils/ProgressHUD.dart';

class AjoutProduit extends StatefulWidget {
  const AjoutProduit({Key? key}) : super(key: key);

  @override
  State<AjoutProduit> createState() => _AjoutState();
}

class _AjoutState extends State<AjoutProduit> {
  bool isAPIcallProcess = false;
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();
  String? nomProduit;
  String? prix;
  String? quantite;
  String? description;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Nouveau Produits"),
        ),
        backgroundColor: Colors.white,
        body: ProgressHUD(
          child: Form(
            key: globalFormKey,
            child: _AjoutUI(context),
          ),
          inAsyncCall: isAPIcallProcess,
          opacity: 0.3,
          key: UniqueKey(),
        ),
      ),
    );
  }

  Widget _AjoutUI(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.only(left: 20, top: 70, bottom: 30),
              child: Text(
                "Nom du produit :",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Colors.black),
              ),
            ),
            FormHelper.inputFieldWidget(
                context, "Nom du produit", "Nom du produit", (onValidateVal) {
              if (onValidateVal.isEmpty) {
                return "Le nom du produit ne peut être vide";
              }
              return null;
            }, (onSaved) {
              nomProduit = onSaved;
            },
                borderFocusColor: Colors.yellow,
                borderColor: Colors.black,
                textColor: Colors.black,
                hintColor: Colors.black.withOpacity(0.8),
                borderRadius: 10),
            const Padding(
              padding: EdgeInsets.only(
                top: 20,
                left: 20,
              ),
              child: Text(
                "Description :",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Colors.black),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: FormHelper.inputFieldWidget(
                context,
                "description",
                "description",
                (onValidateVal) {
                  if (onValidateVal.isEmpty) {
                    return "La description ne peut être vide";
                  }
                  return null;
                },
                (onSaved) {
                  description = onSaved;
                },
                borderFocusColor: Colors.yellow,
                borderColor: Colors.black,
                textColor: Colors.black,
                hintColor: Colors.black.withOpacity(0.8),
                borderRadius: 10,
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(
                top: 20,
                left: 20,
              ),
              child: Text(
                "Quantité :",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Colors.black),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: FormHelper.inputFieldWidget(
                context,
                "quantité",
                "quantité",
                (onValidateVal) {
                  if (onValidateVal.isEmpty) {
                    return "La quantité ne peut être vide";
                  }
                  return null;
                },
                (onSaved) {
                  quantite = onSaved;
                },
                borderFocusColor: Colors.yellow,
                borderColor: Colors.black,
                textColor: Colors.black,
                hintColor: Colors.black.withOpacity(0.8),
                borderRadius: 10,
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(
                top: 20,
                left: 20,
              ),
              child: Text(
                "Prix :",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Colors.black),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: FormHelper.inputFieldWidget(
                context,
                "prix",
                "prix",
                (onValidateVal) {
                  if (onValidateVal.isEmpty) {
                    return "Le prix ne peut être vide";
                  }
                  return null;
                },
                (onSaved) {
                  prix = onSaved;
                },
                borderFocusColor: Colors.yellow,
                borderColor: Colors.black,
                textColor: Colors.black,
                hintColor: Colors.black.withOpacity(0.8),
                borderRadius: 10,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Center(
              child: FormHelper.submitButton("Validez", () {
                dynamic validate = globalFormKey.currentState?.validate();
                if (validate != null && validate) {
                  globalFormKey.currentState?.save();
                  Produits.ajoutProduit(
                      context, nomProduit, description, prix, quantite);
                }
              },
                  btnColor: Colors.yellow,
                  borderColor: Colors.black,
                  txtColor: Colors.black,
                  borderRadius: 10),
            )
          ]),
    );
  }
}
