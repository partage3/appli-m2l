import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutterapp/routes.dart';

import 'modifier_produit.dart';

class AffichageProduits extends StatefulWidget {
  const AffichageProduits({Key? key}) : super(key: key);

  @override
  State<AffichageProduits> createState() => _AffichageProduitsState();
}

class _AffichageProduitsState extends State<AffichageProduits> {
  late Future<List> _bookList;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _bookList = Produits.getAllProduits();
  }

  @override
  Widget build(BuildContext context) {
    if (ModalRoute.of(context)!.settings.arguments != null) {
      Object? arg = ModalRoute.of(context)!.settings.arguments;
      var newProduits = jsonDecode(arg.toString());
      setState(() {
        _bookList = _bookList.then<List>((value) {
          return [newProduits, ...value];
        });
      });
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text("Liste des produits"),
      ),
      body: Container(
        child: FutureBuilder<List>(
          future: _bookList,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: snapshot.data?.length,
                  itemBuilder: (context, i) {
                    return Card(
                      child: ListTile(
                        title: Text(
                          snapshot.data![i]['nomProduit'],
                          style: const TextStyle(fontSize: 30),
                        ),
                        subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Text(snapshot.data![i]['description'],
                                  style: const TextStyle(fontSize: 20)),
                            ]),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            IconButton(
                              icon: Icon(Icons.edit),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ModifierProduit(
                                          ref: int.parse(snapshot.data![i]
                                                  ["reference"]
                                              .toString()))),
                                );
                              },
                            ),
                            IconButton(
                              icon: Icon(Icons.delete),
                              onPressed: () async {
                                int ref = snapshot.data![i]['reference'];
                                print(ref);
                                await Produits.deleteProduit(context, ref);
                                setState(() {
                                  _bookList = _bookList.then<List>((value) {
                                    return value
                                        .where((element) =>
                                            element['reference'] != ref)
                                        .toList();
                                  });
                                });
                              },
                            ),
                          ],
                        ),
                      ),
                    );
                  });
            } else {
              return const Center(
                child: Text("Pas de données"),
              );
            }
          },
        ),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
              onPressed: () {
                Navigator.pushNamed(context, '/ajout');
              },
              child: const Text("Ajouter")),
        ],
      ),
    );
  }
}
