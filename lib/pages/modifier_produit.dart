import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutterapp/routes.dart';
import 'package:http/http.dart' as http;

class ModifierProduit extends StatefulWidget {
  final int ref;

  const ModifierProduit({Key? key, required this.ref}) : super(key: key);

  @override
  State<ModifierProduit> createState() => _ModifierProduitState();
}

class _ModifierProduitState extends State<ModifierProduit> {
  final _formKey = GlobalKey<FormState>();
  late Future<List> _bookList;
  late String _nomProduit;
  late String _description;
  late String _prix;
  late String _quantite;

  @override
  void initState() {
    super.initState();
    _bookList = Produits.getAllProduits();
    _bookList.then((produits) {
      final produit = produits.firstWhere((u) => u['reference'] == widget.ref,
          orElse: () => null);
      if (produit != null) {
        setState(() {
          _nomProduit = produit['nomProduit'];
          _description = produit['description'];
          _prix = produit['prix'].toString();
          _quantite = produit['quantite'].toString();
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Modifier un produit"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const Padding(
                  padding: EdgeInsets.only(left: 20, top: 70, bottom: 30),
                  child: Text(
                    "Nom du produit :",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.black,
                    ),
                  ),
                ),
                TextFormField(
                  initialValue: _nomProduit,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                  ),
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.black),
                  onSaved: (value) {
                    _nomProduit = value!;
                  },
                ),
                const SizedBox(
                  height: 16,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20, top: 20, bottom: 10),
                  child: Text(
                    "Description :",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.black,
                    ),
                  ),
                ),
                TextFormField(
                  initialValue: _description,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      borderSide: BorderSide(
                        color: Colors.black, // Couleur des bordures
                        width: 2.0, // Épaisseur des bordures
                      ),
                    ),
                    labelStyle: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.black,
                    ),
                    hintStyle: TextStyle(
                      fontSize: 25,
                      color: Colors.black.withOpacity(0.8),
                    ),
                  ),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Colors.black,
                  ),
                  onSaved: (value) {
                    _description = value!;
                  },
                ),
                const SizedBox(height: 16),
                const Padding(
                  padding: EdgeInsets.only(left: 20, top: 20, bottom: 10),
                  child: Text(
                    "Quantité :",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 25,
                        color: Colors.black),
                  ),
                ),
                TextFormField(
                  initialValue: _quantite,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                  ),
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.black),
                  keyboardType: TextInputType.number,
                  onSaved: (value) {
                    _quantite = value!;
                  },
                ),
                const SizedBox(height: 24),
                const Padding(
                  padding: EdgeInsets.only(left: 20, top: 20, bottom: 10),
                  child: Text(
                    "Prix :",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 25,
                        color: Colors.black),
                  ),
                ),
                TextFormField(
                  initialValue: _prix,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                  ),
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.black),
                  keyboardType: TextInputType.number,
                  onSaved: (value) {
                    _prix = value!;
                  },
                ),
                const SizedBox(height: 40),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40),
                    child: ElevatedButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          _formKey.currentState!.save();
                          Produits.updateProduit(context, _nomProduit,
                              _description, _prix, _quantite, widget.ref);
                        }
                      },
                      child: const Text('Enregistrer'),
                      style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(
                            vertical: 22, horizontal: 35),
                        textStyle: const TextStyle(fontSize: 18),
                        primary: Colors.yellow,
                        onPrimary: Colors.black,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
