import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutterapp/routes.dart';
import 'package:http/http.dart' as http;

class ModifierUtilisateur extends StatefulWidget {
  final int ref;

  const ModifierUtilisateur({Key? key, required this.ref}) : super(key: key);

  @override
  State<ModifierUtilisateur> createState() => _ModifierUtilisateurState();
}

class _ModifierUtilisateurState extends State<ModifierUtilisateur> {
  final _formKey = GlobalKey<FormState>();
  late Future<List> _bookList;
  late String _nomUtilisateur;
  late String _prenomUtilisateur;
  late String _mail;
  late String
      _selectedRole; // Nouvelle variable pour stocker la valeur sélectionnée du rôle

  @override
  void initState() {
    super.initState();
    _bookList = Utilisateurs.getAllUtilisateurs();
    _bookList.then((users) {
      final user =
          users.firstWhere((u) => u['id'] == widget.ref, orElse: () => null);
      if (user != null) {
        setState(() {
          _nomUtilisateur = user['nomUtilisateur'];
          _prenomUtilisateur = user['prenomUtilisateur'];
          _mail = user['mail'];
          _selectedRole =
              user['role']; // Mettre à jour la valeur sélectionnée du rôle
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Modifier un utilisateur"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const Padding(
                  padding: EdgeInsets.only(left: 20, top: 70, bottom: 30),
                  child: Text(
                    "Nom de l'utilisateur :",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 25,
                        color: Colors.black),
                  ),
                ),
                TextFormField(
                  initialValue: _nomUtilisateur,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                  ),
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.black),
                  onSaved: (value) {
                    _nomUtilisateur = value!;
                  },
                ),
                const Padding(
                  padding: EdgeInsets.only(left: 20, top: 20, bottom: 10),
                  child: Text(
                    "Prénom de l'utilisateur :",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 25,
                        color: Colors.black),
                  ),
                ),
                TextFormField(
                  initialValue: _prenomUtilisateur,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                  ),
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.black),
                  onSaved: (value) {
                    _prenomUtilisateur = value!;
                  },
                ),
                const Padding(
                  padding: EdgeInsets.only(left: 20, top: 20, bottom: 10),
                  child: Text(
                    "Mail de l'utilisateur :",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 25,
                        color: Colors.black),
                  ),
                ),
                TextFormField(
                  initialValue: _mail,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                  ),
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                      color: Colors.black),
                  onSaved: (value) {
                    _mail = value!;
                  },
                ),
                const Padding(
                  padding: EdgeInsets.only(left: 20, top: 20, bottom: 10),
                  child: Text(
                    "Rôle de l'utilisateur :",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 25,
                        color: Colors.black),
                  ),
                ),
                DropdownButtonFormField<String>(
                  value: _selectedRole, // Mettre à jour la valeur sélectionnée
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                  ),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Colors.black,
                  ),
                  onChanged: (String? value) {
                    setState(() {
                      _selectedRole =
                          value!; // Mettre à jour la valeur sélectionnée
                    });
                  },
                  onSaved: (String? value) {
                    _selectedRole =
                        value ?? ''; // Sauvegarder la valeur sélectionnée
                  },
                  items: [
                    DropdownMenuItem<String>(
                      value: 'user',
                      child: Text('Utilisateur'),
                    ),
                    DropdownMenuItem<String>(
                      value: 'admin',
                      child: Text('Administrateur'),
                    ),
                  ],
                ),
                const SizedBox(height: 40),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40),
                    child: ElevatedButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          _formKey.currentState!.save();
                          Utilisateurs.updateUtilisateurs(
                              context,
                              _nomUtilisateur,
                              _prenomUtilisateur,
                              _mail,
                              _selectedRole, // Utiliser la valeur sélectionnée
                              widget.ref);
                        }
                      },
                      child: const Text('Enregistrer'),
                      style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(
                            vertical: 22, horizontal: 35),
                        textStyle: const TextStyle(fontSize: 18),
                        primary: Colors.yellow,
                        onPrimary: Colors.black,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
