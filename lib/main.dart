import 'package:flutter/material.dart';
import 'package:flutterapp/pages/AffichageProduits.dart';
import 'package:flutterapp/pages/AffichageUtilisateurs.dart';
import 'package:flutterapp/pages/AjoutProduit.dart';
import 'package:flutterapp/pages/LoginPage.dart';
import 'package:flutterapp/pages/Accueil.dart';
import 'package:flutterapp/pages/modifier_produit.dart';
import 'package:flutterapp/routes.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'M2L Shop',
      theme: ThemeData(
        primarySwatch: Colors.yellow,
      ),
      routes: {
        '/': (context) => LoginPage(),
        '/acceuil': (context) => Acceuil(),
        '/ajout': (context) => AjoutProduit(),
        '/produits': (context) => AffichageProduits(),
        '/utilisateurs': (context) => AffichageUtilisateurs(),
      },
    );
  }
}
