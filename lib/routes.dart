import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Produits {
  static String baseUrl = "http://localhost:8000";
  static Future<List> getAllProduits() async {
    try {
      var res = await http.get(Uri.parse(baseUrl + '/produits'));
      if (res.statusCode == 200) {
        return jsonDecode(res.body);
      } else {
        return Future.error("erreur serveur");
      }
    } catch (err) {
      return Future.error(err);
    }
  }

  static ajoutProduit(
      BuildContext context, nomProduit, description, prix, quantite) async {
    try {
      Map<String, dynamic> data = {
        "nomProduit": nomProduit,
        "description": description,
        "prix": prix,
        "quantite": quantite
      };

      var res = await http.post(Uri.parse(baseUrl + '/produit'),
          headers: {"Content-Type": "application/json"},
          body: jsonEncode(data));

      if (res.statusCode == 201) {
        Navigator.pushNamed(context, '/produits', arguments: res.body);
      } else {
        Navigator.pushNamed(context, '/produits');
      }
    } catch (err) {
      return Future.error(err);
    }
  }

  static Future<bool> deleteProduit(BuildContext context, int ref) async {
    try {
      final response = await http.delete(Uri.parse(baseUrl + '/produit/$ref'));
      if (response.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      throw Exception('Failed to delete product.');
    }
  }

  static Future<void> updateProduit(
    BuildContext context,
    String nomProduit,
    String description,
    String prix,
    String quantite,
    int ref,
  ) async {
    try {
      double prixFinal = double.parse(prix);
      var res = await http.put(
        Uri.parse(baseUrl + "/produit/$ref"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode({
          'nomProduit': nomProduit,
          'description': description,
          'prix': prixFinal,
          'quantite': quantite,
        }),
      );
      if (res.statusCode == 200) {
        Navigator.pushNamed(context, '/produits');
      } else {
        Navigator.pushNamed(context, '/produits');
      }
    } catch (error) {
      return Future.error(error);
    }
  }
}

class Utilisateurs {
  static String baseUrl = "http://localhost:8000";
  static Future<List> getAllUtilisateurs() async {
    try {
      var res = await http.get(Uri.parse(baseUrl + '/User'));
      if (res.statusCode == 200) {
        return jsonDecode(res.body);
      } else {
        return Future.error("erreur serveur");
      }
    } catch (err) {
      return Future.error(err);
    }
  }

  static Login(BuildContext context, login, password) async {
    try {
      //var connection = {"email": login, "password": password};
      var res = await http.post(Uri.parse("http://localhost:8000/loginA"),
          headers: {
            "Access-Control-Allow-Origin": "*",
            'Content-Type': 'application/json'
          },
          body: json.encode({"mail": login, "mdp": password}));
      if (res.statusCode == 200) {
        Navigator.pushNamed(context, '/acceuil');
      } else {
        Navigator.pushNamed(context, '/');
      }
    } catch (err) {
      return Future.error(err);
    }
  }

  static Future<bool> deleteUtilisateur(BuildContext context, int ref) async {
    try {
      final response = await http.delete(Uri.parse(baseUrl + '/user/$ref'));
      if (response.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      throw Exception('Failed to delete user.');
    }
  }

  static Future<void> updateUtilisateurs(
    BuildContext context,
    String nomUtilisateur,
    String prenomUtilisateur,
    String mail,
    String role,
    int ref,
  ) async {
    try {
      var res = await http.put(
        Uri.parse(baseUrl + "/User/$ref"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode({
          'nomUtilisateur': nomUtilisateur,
          'prenomUtilisateur': prenomUtilisateur,
          'mail': mail,
          'role': role,
        }),
      );
      if (res.statusCode == 200) {
        Navigator.pushNamed(context, '/utilisateurs');
      } else {
        Navigator.pushNamed(context, '/utilisateurs');
      }
    } catch (error) {
      return Future.error(error);
    }
  }
}
